var page = require('webpage').create();

page.open('http://mastersoftwaretechnologies.com/phantom/', function(status) {
    page.onConsoleMessage = function (msg) { console.log(msg); };
    if (status !== 'success') {
        console.log('Unable to access network');
    } else {
         page.evaluate(function() {
            var list = document.querySelectorAll('div.resume');
            for (var i = 0; i < list.length; i++){
               console.log((i + 1) + ":" + list[i].innerText);
            }
         });
    }
    phantom.exit();
});

var page = require('webpage').create(),
    system = require('system'),
    t, address;

page.settings.userName = 'test';
page.settings.password = 'test';

if (system.args.length === 1) {
    console.log('Usage: loadspeed.js http://mastersoftwaretechnologies.com/phantom/');
    phantom.exit();
} else {
    t = Date.now();
    address = system.args[1];
    page.open(address, function (status) {
        if (status !== 'success') {
            console.log('FAIL to load the address');
        } else {
            t = Date.now() - t;
            console.log('Page title is ' + page.evaluate(function () {
                return document.title;
            }));
            console.log('Loading time ' + t + ' msec');
        }
        phantom.exit();
    });
}